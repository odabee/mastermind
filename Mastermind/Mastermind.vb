﻿Imports System.Text

Public Class Mastermind

    Public Property Pieces As String
    Public Property NbPiecesByLine As Integer
    Public Property RulesText As String
    Public Property RuleLines As List(Of Line)
    Public Property NbSoluces As Integer
    Public Property ElapsedTime As Double
    Public Property UniqChars As Boolean

    Private Lignes As Long

    Public Class Line
        Public Property Line As String
        Public Property NbGood As Integer
        Public Property NbBad As Integer
        Public Property NbSolucesLine As Integer
        Public Property ElapsedTimeLine As Single

        Sub New(Line As String, NbGood As Integer, NbBad As Integer)
            Me.Line = Line
            Me.NbGood = NbGood
            Me.NbBad = NbBad
        End Sub
    End Class

    Sub New(Pieces As String, NbPiecesByLine As Integer)
        Me.NbPiecesByLine = NbPiecesByLine
        Me.Pieces = Pieces
        Me.RuleLines = New List(Of Line)
    End Sub

    Public Function Solve(bUniq As Boolean) As List(Of String)

        Dim ListSol As New List(Of String)
        Dim tm As Double = Microsoft.VisualBasic.Timer

        If IsUniq(Pieces) = False Then
            MsgBox("Les caractères ne sont pas uniques...")
        Else
            Call DumpSolve(Pieces, bUniq, 0, "", ListSol)

            Dim tm2 As Double = Microsoft.VisualBasic.Timer
            Me.ElapsedTime = Microsoft.VisualBasic.Timer - tm

            Me.NbSoluces = ListSol.Count
        End If

        Return ListSol

    End Function

    Public Sub DumpSolve(p As String, bUniq As Boolean, level As Integer, Root As String, Soluces As List(Of String))

        Dim li As String = ""

        If level < NbPiecesByLine Then
            For pi As Integer = 0 To p.Length - 1
                Call DumpSolve(p, bUniq, level + 1, Root & p(pi), Soluces)
            Next
        Else
            Dim CntOk As Integer = 0
            Dim NbGood As Integer = 0, NbBad As Integer = 0

            For Each l As Line In RuleLines
                Dim bSearch As Boolean = True
                If bUniq Then
                    bSearch = IsUniq(Root)
                End If
                If bSearch Then
                    Call CheckPlaces(New StringBuilder(Root), New StringBuilder(l.Line), NbGood, NbBad)
                    If NbGood = l.NbGood And NbBad = l.NbBad Then
                        CntOk += 1
                    Else
                        Exit For
                    End If
                End If
            Next
            If CntOk = RuleLines.Count Then
                'Debug.Print("Solution : " & Root)
                Soluces.Add(Root)
            End If
        End If

    End Sub

    Public Sub CheckPlaces(Sol As StringBuilder, TrySol As StringBuilder, ByRef NbGood As Integer, ByRef NbBad As Integer)
        NbGood = 0
        NbBad = 0
        For i As Integer = 0 To Sol.Length - 1
            If Sol(i) = TrySol(i) Then
                NbGood += 1
                Sol(i) = "_"
                TrySol(i) = "_"
            End If
        Next
        For i As Integer = 0 To Sol.Length - 1
            If TrySol(i) <> "_" Then
                For j = 0 To Sol.Length - 1
                    If TrySol(i) = Sol(j) Then
                        NbBad += 1
                        Sol(j) = "_"
                        TrySol(i) = "_"
                        Exit For
                    End If
                Next
            End If
        Next
    End Sub

    Public Function IsUniq(Sol As String) As Boolean
        Dim bRet As Boolean = True

        For i As Integer = 0 To Sol.Length - 1
            Dim c As Char = Sol(i)
            Sol = Replace(Sol, c, "_")
            If InStr(1, Sol, c) > 0 Then
                bRet = False
                Exit For
            End If
        Next
        Return (bRet)

    End Function

    Public Sub Dump(Lb As ListBox, level As Integer, Root As String)

        If level = 0 Then
            Lignes = 0
        End If
        Dim li As String = ""
        If level < NbPiecesByLine Then
            For pi As Integer = 0 To Pieces.Length - 1
                Call Dump(Lb, level + 1, Root & Pieces(pi))
            Next
        Else
            Lb.Items.Add("Level=" & level & " Ligne=" & Lignes & " " & Root)
            Lignes += 1
        End If

    End Sub

    Public Sub TDD(Lb As ListBox)

        'Call m.Dump(m.Pieces, 0, "")
        'Call m.DumpS(m.Pieces, 0, Nothing)
        'ListBox1.Items.Add("Lignes=" & m.Lignes)

        Lb.Items.Clear()

        Lb.Items.Add("TDD ------------")
        Dim Nbg%, Nbb%
        Call Me.CheckPlaces(New StringBuilder("ABC"), New StringBuilder("ABC"), Nbg, Nbb)
        Lb.Items.Add("NbG=" & Nbg & " Nbb=" & Nbb)
        Debug.Assert(Nbg = 3 And Nbb = 0)

        Call Me.CheckPlaces(New StringBuilder("ABC"), New StringBuilder("CAB"), Nbg, Nbb)
        Lb.Items.Add("NbG=" & Nbg & " Nbb=" & Nbb)
        Debug.Assert(Nbg = 0 And Nbb = 3)

        Call Me.CheckPlaces(New StringBuilder("ABC"), New StringBuilder("XYZ"), Nbg, Nbb)
        Lb.Items.Add("NbG=" & Nbg & " Nbb=" & Nbb)
        Debug.Assert(Nbg = 0 And Nbb = 0)

        Call Me.CheckPlaces(New StringBuilder("ABC"), New StringBuilder("AAB"), Nbg, Nbb)
        Lb.Items.Add("NbG=" & Nbg & " Nbb=" & Nbb)
        Debug.Assert(Nbg = 1 And Nbb = 1)

        Call Me.CheckPlaces(New StringBuilder("AAB"), New StringBuilder("AAB"), Nbg, Nbb)
        Lb.Items.Add("NbG=" & Nbg & " Nbb=" & Nbb)
        Debug.Assert(Nbg = 3 And Nbb = 0)

        Call Me.CheckPlaces(New StringBuilder("AAB"), New StringBuilder("BAA"), Nbg, Nbb)
        Lb.Items.Add("NbG=" & Nbg & " Nbb=" & Nbb)
        Debug.Assert(Nbg = 1 And Nbb = 2)

        Lb.Items.Add("END OF TDD")

    End Sub

    Public Sub Save()
        My.Computer.Registry.CurrentUser.SetValue("Pieces", Me.Pieces)
        My.Computer.Registry.CurrentUser.SetValue("NbPiecesByLine", Me.NbPiecesByLine)
        My.Computer.Registry.CurrentUser.SetValue("UniqChars", Me.UniqChars)
        My.Computer.Registry.CurrentUser.SetValue("RulesText", Me.RulesText)
    End Sub

    Public Shared Function Load() As Mastermind
        Dim Pieces As String = My.Computer.Registry.CurrentUser.GetValue("Pieces")
        If Pieces <> "" Then
            Dim NbPiecesByLine As Integer = My.Computer.Registry.CurrentUser.GetValue("NbPiecesByLine")
            Dim mRet As New Mastermind(Pieces, NbPiecesByLine)
            mRet.UniqChars = My.Computer.Registry.CurrentUser.GetValue("UniqChars")
            mRet.RulesText = My.Computer.Registry.CurrentUser.GetValue("RulesText")
            Return mRet
        Else
            Return Nothing
        End If
    End Function

End Class
