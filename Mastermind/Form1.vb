﻿Imports System.Text

Public Class Form1
    Dim m As Mastermind

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        Dim Soluces As New List(Of String)
        Dim LineIndex As Integer = 0

        ' https://www.enigme-facile.fr/enigme-cadenas-olicard-11336
        ' https://www.enigme-facile.fr/enigme-cadenas-olicard-2-11338

        m = New Mastermind(TextBoxChars.Text, Val(TextBoxDigits.Text))

        m.RulesText = TextBoxRules.Text
        m.RuleLines.Clear()

        m.UniqChars = CheckBoxUniq.Checked

        For Each line As String In TextBoxRules.Lines
            line = line.Trim
            If line.Length > 0 Then
                Dim Arr As String() = line.Split(",")
                If Arr.Length <> 3 Then
                    MsgBox("Il faut 3 informations séparées par une virgule : Code,Nb bien placés, Nb Mal placés")
                    Exit Sub
                End If
                If Arr(0).Length <> Val(TextBoxDigits.Text) Then
                    MsgBox("Il faut que le code '" & Arr(0) & "' soit de " & Val(TextBoxDigits.Text) & " Caractères")
                    Exit Sub
                End If
                m.RuleLines.Add(New Mastermind.Line(Arr(0), Val(Arr(1)), Val(Arr(2))))
                m.Solve(CheckBoxUniq.Checked)
                m.RuleLines(LineIndex).NbSolucesLine = m.NbSoluces
                m.RuleLines(LineIndex).ElapsedTimeLine = m.ElapsedTime
                LineIndex += 1
            End If
        Next
        ListBox1.Items.Clear()

        For Each l As Mastermind.Line In m.RuleLines
            ListBox1.Items.Add(l.Line & " Good=" & l.NbGood & " Bad=" & l.NbBad & " NbSoluces=" & l.NbSolucesLine & " (" & Format(l.ElapsedTimeLine, "0.0000") & ") sec.")
        Next
        ListBox1.Items.Add("----------------------")

        Soluces = m.Solve(CheckBoxUniq.Checked)
        If Soluces.Count > 0 Then
            ListBox1.Items.Add(Soluces.Count & " solution(s) (" & Format(m.ElapsedTime, "0.0000") & ") sec.")
            ListBox1.Items.Add("----------------------")
            For Each Soluce As String In Soluces
                ListBox1.Items.Add("Solution " & Soluce)
            Next
        Else
            ListBox1.Items.Add("Pas de solution !!!")
        End If

        m.Save()

    End Sub

    Private Sub ButtonEx1_Click(sender As Object, e As EventArgs) Handles ButtonEx1.Click

        TextBoxChars.Text = "0123456789"
        TextBoxDigits.Text = "3"
        CheckBoxUniq.Checked = False
        ListBox1.Items.Clear()
        TextBoxRules.Text = "684,1,0
                             621,0,1
                             436,0,2
                             708,0,0
                             783,0,1".Replace(" ", "")

    End Sub

    Private Sub ButtonEx2_Click(sender As Object, e As EventArgs) Handles ButtonEx2.Click

        TextBoxChars.Text = "0123456789"
        TextBoxDigits.Text = "4"
        CheckBoxUniq.Checked = True
        ListBox1.Items.Clear()
        TextBoxRules.Text = "7619,0,2
                             4763,0,0
                             0451,0,1
                             5942,2,0".Replace(" ", "")
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim m As Mastermind = Mastermind.Load()
        If m Is Nothing Then
            ButtonEx1.PerformClick()
        Else
            TextBoxChars.Text = m.Pieces
            TextBoxDigits.Text = m.NbPiecesByLine
            TextBoxRules.Text = m.RulesText
            CheckBoxUniq.Checked = m.UniqChars
            ListBox1.Items.Clear()
        End If

    End Sub

    Private Sub ButtonTDD_Click(sender As Object, e As EventArgs) Handles ButtonTDD.Click
        Dim m As New Mastermind("0123456789", 3)

        m.TDD(Me.ListBox1)
    End Sub

    Private Sub ButtonComb_Click(sender As Object, e As EventArgs) Handles ButtonComb.Click

        m = New Mastermind(TextBoxChars.Text, Val(TextBoxDigits.Text))
        Me.ListBox1.Items.Clear()
        Me.ListBox1.Refresh()
        Call m.Dump(Me.ListBox1, 0, "")

    End Sub

End Class
