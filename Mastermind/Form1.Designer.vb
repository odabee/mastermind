﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.TextBoxChars = New System.Windows.Forms.TextBox()
        Me.TextBoxDigits = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LabelC = New System.Windows.Forms.Label()
        Me.TextBoxRules = New System.Windows.Forms.TextBox()
        Me.CheckBoxUniq = New System.Windows.Forms.CheckBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ButtonEx1 = New System.Windows.Forms.Button()
        Me.ButtonEx2 = New System.Windows.Forms.Button()
        Me.ButtonTDD = New System.Windows.Forms.Button()
        Me.ButtonComb = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(128, 167)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Décode"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'ListBox1
        '
        Me.ListBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(12, 196)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(288, 95)
        Me.ListBox1.TabIndex = 1
        '
        'TextBoxChars
        '
        Me.TextBoxChars.Location = New System.Drawing.Point(12, 28)
        Me.TextBoxChars.Name = "TextBoxChars"
        Me.TextBoxChars.Size = New System.Drawing.Size(148, 20)
        Me.TextBoxChars.TabIndex = 2
        Me.TextBoxChars.Text = "0123456789"
        '
        'TextBoxDigits
        '
        Me.TextBoxDigits.Location = New System.Drawing.Point(180, 27)
        Me.TextBoxDigits.Name = "TextBoxDigits"
        Me.TextBoxDigits.Size = New System.Drawing.Size(60, 20)
        Me.TextBoxDigits.TabIndex = 3
        Me.TextBoxDigits.Text = "3"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(177, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Nb de Digits"
        '
        'LabelC
        '
        Me.LabelC.AutoSize = True
        Me.LabelC.Location = New System.Drawing.Point(12, 12)
        Me.LabelC.Name = "LabelC"
        Me.LabelC.Size = New System.Drawing.Size(104, 13)
        Me.LabelC.TabIndex = 5
        Me.LabelC.Text = "Caractères possibles"
        '
        'TextBoxRules
        '
        Me.TextBoxRules.Location = New System.Drawing.Point(12, 103)
        Me.TextBoxRules.Multiline = True
        Me.TextBoxRules.Name = "TextBoxRules"
        Me.TextBoxRules.Size = New System.Drawing.Size(111, 87)
        Me.TextBoxRules.TabIndex = 6
        '
        'CheckBoxUniq
        '
        Me.CheckBoxUniq.AutoSize = True
        Me.CheckBoxUniq.Location = New System.Drawing.Point(12, 54)
        Me.CheckBoxUniq.Name = "CheckBoxUniq"
        Me.CheckBoxUniq.Size = New System.Drawing.Size(168, 17)
        Me.CheckBoxUniq.TabIndex = 7
        Me.CheckBoxUniq.Text = "Caractères uniques seulement"
        Me.CheckBoxUniq.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(13, 84)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(225, 13)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Règles : Code, Nb bien placée, Nb mal placés"
        '
        'ButtonEx1
        '
        Me.ButtonEx1.Location = New System.Drawing.Point(128, 103)
        Me.ButtonEx1.Name = "ButtonEx1"
        Me.ButtonEx1.Size = New System.Drawing.Size(93, 24)
        Me.ButtonEx1.TabIndex = 9
        Me.ButtonEx1.Text = "Exemple Niv. 1"
        Me.ButtonEx1.UseVisualStyleBackColor = True
        '
        'ButtonEx2
        '
        Me.ButtonEx2.Location = New System.Drawing.Point(128, 133)
        Me.ButtonEx2.Name = "ButtonEx2"
        Me.ButtonEx2.Size = New System.Drawing.Size(93, 24)
        Me.ButtonEx2.TabIndex = 10
        Me.ButtonEx2.Text = "Exemple Niv. 2"
        Me.ButtonEx2.UseVisualStyleBackColor = True
        '
        'ButtonTDD
        '
        Me.ButtonTDD.Location = New System.Drawing.Point(227, 104)
        Me.ButtonTDD.Name = "ButtonTDD"
        Me.ButtonTDD.Size = New System.Drawing.Size(49, 23)
        Me.ButtonTDD.TabIndex = 11
        Me.ButtonTDD.Text = "TDD"
        Me.ButtonTDD.UseVisualStyleBackColor = True
        '
        'ButtonComb
        '
        Me.ButtonComb.Location = New System.Drawing.Point(227, 133)
        Me.ButtonComb.Name = "ButtonComb"
        Me.ButtonComb.Size = New System.Drawing.Size(49, 23)
        Me.ButtonComb.TabIndex = 12
        Me.ButtonComb.Text = "Comb."
        Me.ButtonComb.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(307, 301)
        Me.Controls.Add(Me.ButtonComb)
        Me.Controls.Add(Me.ButtonTDD)
        Me.Controls.Add(Me.ButtonEx2)
        Me.Controls.Add(Me.ButtonEx1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.CheckBoxUniq)
        Me.Controls.Add(Me.TextBoxRules)
        Me.Controls.Add(Me.LabelC)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBoxDigits)
        Me.Controls.Add(Me.TextBoxChars)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.Button1)
        Me.Name = "Form1"
        Me.Text = "Code Solver"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button1 As Button
    Friend WithEvents ListBox1 As ListBox
    Friend WithEvents TextBoxChars As TextBox
    Friend WithEvents TextBoxDigits As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents LabelC As Label
    Friend WithEvents TextBoxRules As TextBox
    Friend WithEvents CheckBoxUniq As CheckBox
    Friend WithEvents Label2 As Label
    Friend WithEvents ButtonEx1 As Button
    Friend WithEvents ButtonEx2 As Button
    Friend WithEvents ButtonTDD As Button
    Friend WithEvents ButtonComb As Button
End Class
